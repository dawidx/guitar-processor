#include <stdio.h>
#include <math.h>

int g_Array[40];

float Sine_Wave(int frequency, bool reset) {
	static float time;
	if (reset) {
		time = 0;
	}
	time += 2 * 3.14195 / (44100/frequency);
	return sin(time)*128+128;
}

int Get_HighPass_Index_Offset(int offset, int index, bool substract) {
	if (substract) {
		if ((index - offset) < 0) {
			return (index-offset) + 20;
		}
		else {
			return (index - offset);
		}
	} 
	else {

		if ((offset + index) >= 20) {
			return (offset + index) - 20;
		}
		else {
			return index + offset;
		}
	}
}

int Get_HighPass_Value(int value) {
	static int buffer[20];
	static int index;
	//Simple recursive low-pass filter
	//Storing the value in the buffer for the future reference
	//index works as a 0 value. 
	buffer[index] = value;
	//Determining the time slices
	float dt = 1.0f/44100.0f;
	printf("dt: %f \n", dt);
	
	//Standard equation for calculating breakpoint frequency RC = 1/(2*PI*Fb) where Fb is the breakpoint frequency
	float RC = 1.0f / (2.0f * 3.14159f * 500.0f) ;
	printf("RC: %f\n", RC);
	//Alpha is defined as time constant 
	float alpha = RC / (RC + dt);
	printf("Alpha: %f\n", alpha);
	//Exponentially moving average. First order differential equation
	float new_value[20];
	//The last value in the ring buffer
	new_value[0] = (float)buffer[Get_HighPass_Index_Offset(1, index, false)];
	//Analysing the last 20 values. to determin the slope
	int i;
	for (i = 1; i < 20; i++) {
		new_value[i] = (alpha * ((float)new_value[i - 1] + (float)buffer[Get_HighPass_Index_Offset(18 - i, index, true)] - (float)buffer[Get_HighPass_Index_Offset(19 - i, index, true)]));
		printf("19-%d: %d\n", i, buffer[Get_HighPass_Index_Offset(19 - i, index, true)]);
		printf("18-%d: %d\n", i, buffer[Get_HighPass_Index_Offset(18 - i, index, true)]);
		printf("New Value[%d]: %f\n", i, new_value[i]);
	}
	//Increment index
	index = Get_HighPass_Index_Offset(1, index, false);
	//returning the last value
	//printf("Index: %d\n", index);
	return (int)new_value[19];
}

int main() {
	int i;
	//preparing the data
	int fail = 0;
	int pass = 0;
	float sine;
	int value;
	float mean = 0;
	//for (int k = 1; k < 22050; k *= 2) {
	int k = 20000;
		bool reset = true;
		for (i = 0; i < 44100; i++) {
			sine = Sine_Wave(k, reset);
			value = Get_HighPass_Value((int)sine);
			mean = (mean + ((int)sine - value)) / 2;
			reset = false;
		}

		printf("%dHz: %f\t ", k, mean);
	//}
	/*
	for (i = 0; i < 44100; i++) {
		sine = Sine_Wave(30);
		//printf("Input: %d \n", (int)sine);
		value = Get_HighPass_Value((int)sine);
		mean2 = (mean2 + ((int)sine - value)) / 2;
	}

	for (i = 0; i < 44100; i++) {
		sine = Sine_Wave(490);
		//printf("Input: %d \n", (int)sine);
		value = Get_HighPass_Value((int)sine);
		mean3 = (mean3 + ((int)sine - value)) / 2;
	}
	*/
	int d;
	scanf("%d", &d);
	return 0;
}