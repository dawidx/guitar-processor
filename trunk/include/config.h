/**
 * @desc Contains global defintions and settings for the running of the program.
 */

#define DEBUG false
// Defining the Maximum number of elements in series and parallel. Current UI implementation
// requires MAX_SERIES and MAX_PARRALLEL to be 3.
#define MAX_SERIES 4
#define MAX_PARALLEL 4

// Defining minimum sampling rate
#define MIN_SAMPLING_RATE 100
#define MAX_SAMPLING_RATE 44100
// #define MAX_SAMPLING_RATE 100000

#define PI 3.14159265

// Defining ID for each filter
#define PASSIVE_EFFECT_ID 0
#define DISTORTION_EFFECT_ID 1
#define CHORUS_EFFECT_ID 2
#define ECHO_EFFECT_ID 3
#define DELAY_EFFECT_ID 4
#define LOWPASS_EFFECT_ID 5
#define HIGHPASS_EFFECT_ID 6

//
 #define NOISE_GATE_PERCENT 15
 #define SPIKE_GATE_PERCENT 30

// Array of effects for use of engine.c
uint8_t g_AvailableEffects[];

/* The sample rate of the effects chain, modifiable at run time to ensure that the computation
*  completes
*/
uint32_t g_SampleRate;

/* Use looping buffer, the buffer starts at 
*  g_InputBuffer[g_InputBufferStart], loops from 
*  g_InputBuffer[BUFFERSIZE] to g_InputBuffer[0] then
*  carries on to g_InputBuffer[g_InputBufferStart - 1].
*  The most recent sample is stored at 
*  g_InputBuffer[g_InputBufferStart], and the oldest at
*  g_InputBuffer[g_InputBufferStart-1].
*/
//uint8_t g_InputBuffer[BUFFERSIZE];
//uint16_t g_InputBufferStart;

// Boolean value to describe whether the effects chain has successfully completed.
// Set to 0 at after input, set to 1 when output complete, set to 2 when effects computation complete.
uint8_t effectsComplete;

// For Debugging
char debugMessageBuffer[0xFF];
int debugMessageLength;