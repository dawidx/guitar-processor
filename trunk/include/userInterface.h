/**
 * @file This file contains all the enumerations and function prototypes for
 * the user interface's functionality.
 */

/**
 * @desc Enumeration for the current state of the user interface.
 */
typedef enum state
{
	// 01
	// 1: Volume
	// 2: Effects
	st_TopLevel,

	// 02
	// menuOption is the current volume
	st_Volume,

	// 03
	// 1: Channel 1
	// 2: Channel 2
	// 3: Channel 3
	// 4: Channel 4
	st_Effects,

	// 1: Effect 1
	// 2: Effect 2
	// 3: Effect 3
	// 4: Effect 4
	st_ch1, // 04
	st_ch2, // 05
	st_ch3, // 06
	st_ch4, // 07

	// 1: Choose Effect
	// 2: Configure Effect
	// 3: Reset Effect Defaults
	// 4: Clear Effect
	st_ch1ef1, // 08
	st_ch1ef2, // 09
	st_ch1ef3, // 10
	st_ch1ef4, // 11
	st_ch2ef1, // 12
	st_ch2ef2, // 13
	st_ch2ef3, // 14
	st_ch2ef4, // 15
	st_ch3ef1, // 16
	st_ch3ef2, // 17
	st_ch3ef3, // 18
	st_ch3ef4, // 19
	st_ch4ef1, // 20
	st_ch4ef2, // 21
	st_ch4ef3, // 22
	st_ch4ef4, // 23

	// Choose effect, menuOption is selected effect index
	st_ch1ef1choose, // 24
	st_ch1ef2choose, // 25
	st_ch1ef3choose, // 26
	st_ch1ef4choose, // 27
	st_ch2ef1choose, // 28
	st_ch2ef2choose, // 29
	st_ch2ef3choose, // 30
	st_ch2ef4choose, // 31
	st_ch3ef1choose, // 32
	st_ch3ef2choose, // 33
	st_ch3ef3choose, // 34
	st_ch3ef4choose, // 35
	st_ch4ef1choose, // 36
	st_ch4ef2choose, // 37
	st_ch4ef3choose, // 38
	st_ch4ef4choose, // 39

	// Configure effect, menuOption is selected effect variable index,
	// menuOption's most significant bit is set to 1 if the variable is
	// being edited, and 0 if it is the variable to edit is being modified	
	st_ch1ef1configure, // 40
	st_ch1ef2configure, // 41
	st_ch1ef3configure, // 42
	st_ch1ef4configure, // 43
	st_ch2ef1configure, // 44
	st_ch2ef2configure, // 45
	st_ch2ef3configure, // 46
	st_ch2ef4configure, // 47
	st_ch3ef1configure, // 48
	st_ch3ef2configure, // 49
	st_ch3ef3configure, // 50
	st_ch3ef4configure, // 51
	st_ch4ef1configure, // 52
	st_ch4ef2configure, // 53
	st_ch4ef3configure, // 54
	st_ch4ef4configure, // 55
} State;

typedef struct menu_configuration
{
	State state;
	uint8_t menuOption;
} Menu_Configuration;

void UI_init();
void checkUI();
void applyAction(char input);
void printUI(Menu_Configuration menuConfig);