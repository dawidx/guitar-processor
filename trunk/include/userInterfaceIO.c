/**
 * @file Functionality interacting with the low level i2c calls to provided keypad input
 * and LCD output functionality
 */
#include "userInterfaceIO.h"
#include "lpc17xx_uart.h"		// Central include files
#include "lpc17xx_pinsel.h"
#include "lpc_types.h"
#include "lpc17xx_i2c.h"
#include "debug_frmwrk.h"		// Debug messages
#include <stdio.h>

#define I2CDEV_S_ADDR_LCD	(0x3B)	// LCD Display Controller address
#define I2CDEV_S_ADDR_KEYPAD (0x21) // Keypad interface address
#define I2CDEV_M LPC_I2C1		// I2C Interface

/**
* @global g_KeyPadInput
* @desc The most recent character recieved from the keypad.
**/
char g_KeyPadInput = ' ';

/**
* @global g_KeyPadTransferComplete
* @desc Set to false when requesting the keypad for data, and true when the data has been recieved.
**/
bool g_KeyPadTransferComplete = true;

/**
* @global g_KeypadRecieveBuffer
* @desc The buffer keypad data is saved to when it is recieved across i2c.
**/
uint8_t g_KeypadRecieveBuffer[1];

/**
 * The buffer containing the location of LCD display cursor, after calling NewLine. Set to 0xFF when unused.
 */
uint8_t g_LCDNewLineRecieveBuffer[1];

/**
* @global complete
* @desc SET when transferring data, RESET when transfer has completed.
**/
__IO FlagStatus complete;

/**
* @global g_i2c_transferStatus
* @desc Status of the i2c transfer. Initialised to NO_TRANSFER, calls to i2c can only be made
* (by convention here) when the status returns to NO_TRANSFER.
**/
i2c_transferStatus_t g_i2c_transferStatus = NO_TRANSFER;

/**
 * Stores the current status of the i2c device in the g_i2c_transferStatus variable. Also deals with the complete flag.
 * @param status The desired i2c status.
 */
void setI2CTransferStatus(i2c_transferStatus_t status)
{
	g_i2c_transferStatus = status;
	if (status == NO_TRANSFER)
	{
		
	}
	else
	{
		complete = RESET;
	}
}

/**
 * Gets the current g_i2c_transferStatus.
 * @return The current transfer status of the i2c device.
 */
i2c_transferStatus_t getI2CTransferStatus()
{
	return g_i2c_transferStatus;
}

/**
 * Blocks until the i2c transfer status is NO_TRANSFER, then set it to the status passed in the parameter.
 * @param status The desired i2c status.
 */
void blockAndSetI2CTransferStatus(i2c_transferStatus_t status)
{
	while (getI2CTransferStatus() != NO_TRANSFER);

	g_i2c_transferStatus = status;
}

/**
 * Interrupt handler for the i2c device. Checks what the current i2c transfer status is (to see if it
 * needs to analyse returned keypad data or not), sets the transfer status to NO_TRANSFER (as the transfer
 * has completed), and if required sets the g_KeyPadInput variable to a suitable value.
 */
void I2C1_IRQHandler(void)
{
	if (DEBUG)
	{
		_DBG("I2C Interrupt\n\r");
	}
	
	// I2C_MasterHandler(I2CDEV_M);
	
	// if (I2C_MasterTransferComplete(I2CDEV_M))
	{

		complete = SET;

		if (DEBUG)
		{
			_DBG("\tI2C Transfer Complete, Calling Callback\n\r");
		}

		if(getI2CTransferStatus() == LCD_TRANSFER)
		{
			LCDTransferCallback();
		}
		else if(getI2CTransferStatus() == KEYPAD_TRANSFER)
		{
			KeypadTransferCallback();
		}
	}
	// else
	// {
	// 	if (DEBUG)
	// 	{
	// 		_DBG("\tI2C Handle Incomlete\n\r");
	// 	}
	// }	
}

/**
 * Called when LCD data has been recieved on i2c, and needs dealing with.
 */
void LCDTransferCallback(void)
{
	if (DEBUG)
	{
		_DBG("\t\tLCD Transfer Callback\n\r");
	}

	if (g_LCDNewLineRecieveBuffer[0] == 0xFF)
	{
		setI2CTransferStatus(NO_TRANSFER);
	}
	else
	{

		if (DEBUG)
		{
			_DBG("\t\t\tNew Line Recieved Current Location\n\r");
		}

		setI2CTransferStatus(NO_TRANSFER);

		uint8_t Transmit_Buffer[2];

		// Current cursor address = g_LCDNewLineRecieveBuffer[0]
		if (g_LCDNewLineRecieveBuffer[0] < 0x40)
		{
			// Set address to 40;
			Transmit_Buffer[1] = 0xC0;	// Go to position 40

		}
		else
		{
			// Set address to 0 / go home
			Transmit_Buffer[1] = 0x80;	// Go to position 0
		}

		g_LCDNewLineRecieveBuffer[0] = 0xFF;

		Transmit_Buffer[0] = 0x00;
		sendDataI2C(Transmit_Buffer, 2, LCD_TRANSFER, NULL, 0);
	}
}

/**
 * Called when keypad data has been recieved on i2c, and needs dealing with.
 */
void KeypadTransferCallback(void)
{
	if (DEBUG)
	{
		_DBG("\t\tKeypad Transfer Callback\n\r");
	}

	g_KeyPadTransferComplete = true;

	g_KeyPadInput = keypadCharFromCode(g_KeypadRecieveBuffer[0]);

	static char previousPress[]={' ',' ',' ',' '}; // Previous press for each column
	
	uint8_t row;
	switch(g_KeyPadInput && 0xF0)
	{
		case(0x70):
			row = 0;
			break;
		case(0xb0):
			row = 1;
			break;

		case(0xd0):
			row = 2;
			break;
		case(0xe0):
			row = 3;
			break;
		default:
			row = 0;
			break;
	}

	if ((previousPress[row] != ' ') || (previousPress[row] == g_KeyPadInput))
	{
		g_KeyPadInput = ' ';
	}

	// if(DEBUG)
	// {
	// 	_DBG("\t\tKeypad Input: \"");
	// 	_DBC(g_KeyPadInput);
	// 	_DBG("\"\n\r");
	// }

	setI2CTransferStatus(NO_TRANSFER);

}

/**
 * Calls the interrupt handler for the i2c device 1 (actually used here)
 */
void I2C0_IRQHandler(void)
{
	if(DEBUG)
		_DBG("Int on I2C device 0\n\r");
	I2C1_IRQHandler();
}

/**
 * Calls the interrupt handler for the i2c device 1 (actually used here)
 */
void I2C2_IRQHandler(void)
{
	if(DEBUG)
		_DBG("Int on I2C device 2\n\r");
	I2C1_IRQHandler();
}

/**
 * Initialises the i2c device.
 */
void i2c_init(void)
{
	PINSEL_CFG_Type PinCfg; // Pin configuration
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;

	if (I2CDEV_M  == LPC_I2C0) // Doesn't work on this hardware.
	{
		// Interrupt setup
		// Disable I2C0 interrupt
		NVIC_DisableIRQ(I2C0_IRQn);
		// preemption = 1, sub-priority = 1
		NVIC_SetPriority(I2C0_IRQn, ((0x01<<3)|0x01));

		// I2C Device Pins
		// Doesn't work
		PinCfg.Funcnum = 3;
		PinCfg.Portnum  = 0;
		PinCfg.Pinnum = 1;
		PINSEL_ConfigPin(&PinCfg);
		PinCfg.Pinnum = 0;
		PINSEL_ConfigPin(&PinCfg);
	}

	if (I2CDEV_M  == LPC_I2C1) // Used here.
	{
		// Interrupt setup
		// Disable I2C0 interrupt
		NVIC_DisableIRQ(I2C1_IRQn);
		// preemption = 1, sub-priority = 1
		NVIC_SetPriority(I2C1_IRQn, ((0x01<<3)|0x01));

		// I2C Device Pins
		PinCfg.Funcnum = 3;
		PinCfg.Portnum  = 0;
		PinCfg.Pinnum = 1;
		PINSEL_ConfigPin(&PinCfg);
		PinCfg.Pinnum = 0;
		PINSEL_ConfigPin(&PinCfg);
	}

	// Works, but incorrect device for using the keypad and lcd display on our hardware setup.
	if (I2CDEV_M  == LPC_I2C2)
	{
		// Interrupt setup
		// Disable I2C0 interrupt
		NVIC_DisableIRQ(I2C2_IRQn);
		// preemption = 1, sub-priority = 1
		NVIC_SetPriority(I2C2_IRQn, ((0x01<<3)|0x01));

		// I2C Device Pins
		PinCfg.Funcnum = 2;
		PinCfg.Portnum  = 0;
		PinCfg.Pinnum = 10;
		PINSEL_ConfigPin(&PinCfg);
		PinCfg.Pinnum = 11;
		PINSEL_ConfigPin(&PinCfg);
	}
	// Initialize Master I2C peripheral, max freq: 100000
	// Not to be confused with a recursive function call (i2c_init)
    I2C_Init(I2CDEV_M, 10);

	// Enable Master I2C operation
    I2C_Cmd(I2CDEV_M, ENABLE);

	// if (I2CDEV_M  == LPC_I2C0) // Doesn't work on current hardware, kept for completeness
	// {
 //    	NVIC_EnableIRQ(I2C0_IRQn);
 //    }
 //    if (I2CDEV_M  == LPC_I2C1) // Used here
	// {
 //    	NVIC_EnableIRQ(I2C1_IRQn);
 //    }
 //    if (I2CDEV_M  == LPC_I2C2) // Works but wrong i2c channel for keypad & LCD display
	// {
 //    	NVIC_EnableIRQ(I2C2_IRQn);
 //    }
}

/**
 * Initialise the LCD device. This function is called once the i2c device has been setup.
 */
void LCD_init(void)
{
	// Buffer for transmission data
	uint8_t Transmit_Buffer[0x12];

	// Fill buffer with data
	
	Transmit_Buffer[0] = 0x04;	// Control: Function Set
	Transmit_Buffer[1] = 0x34;	// Function Set
	Transmit_Buffer[2] = 0x0D;	// Display & Cursor On
	Transmit_Buffer[3] = 0x06;	// Entry Mode Set
	Transmit_Buffer[4] = 0x35;
	Transmit_Buffer[5] = 0x04;
	Transmit_Buffer[6] = 0x10;
	Transmit_Buffer[7] = 0x42; // HV Stages 3
	Transmit_Buffer[8] = 0x9f; // set Vlcd, store to VA
	Transmit_Buffer[9] = 0x34; // DL: 8 bits, M:  two line, SL: 1:18, H: normal instruction set
	Transmit_Buffer[10] = 0x80; // DDRAM Address set to 0x00
	Transmit_Buffer[11] = 0x02; // return home

	// Send data
	if(DEBUG)
		_DBG("Begin transmit LCD initialisation data\n\r");
	sendDataI2C(Transmit_Buffer, 12, LCD_TRANSFER, NULL, 0);
	if(DEBUG)
		_DBG("End transmit LCD initialisation data\n\r");
}

/**
 * Sends data to an i2c device.
 * @param  Transmit_Buffer The data to send to the device.
 * @param  length          The length of the data to send to the device.
 * @param  device          The device to send the data to.
 * @param  *recieve 	   Pointer to the recieve buffer. NULL if transmitting only.
 * @return                 True if successful
 */
bool sendDataI2C(uint8_t *Transmit_Buffer, int transmit_length, i2c_transferStatus_t device, uint8_t *recieve, uint8_t recieveLength)
{
	I2C_M_SETUP_Type transferMCfg;

	if (device == LCD_TRANSFER)
	{
		transferMCfg.sl_addr7bit = I2CDEV_S_ADDR_LCD;
	}
	else if(device == KEYPAD_TRANSFER)
	{
		transferMCfg.sl_addr7bit = I2CDEV_S_ADDR_KEYPAD;
	}
	else
	{
		// If NO_TRANSFER selected, just return.
		return false;
	}



	if (recieve == NULL)
	{
		recieveLength = 0;
	}

	transferMCfg.tx_data = Transmit_Buffer;
	transferMCfg.tx_length = transmit_length;
	transferMCfg.rx_data = recieve;
	transferMCfg.rx_length = recieveLength;
	transferMCfg.retransmissions_max = 3;

	if(DEBUG)
	{
		_DBG("Begin blocking and changing I2C status\n\r");
	}
	blockAndSetI2CTransferStatus(device);

	if(DEBUG)
	{
		_DBG("Transfer data now\n\r");
	}
	I2C_MasterTransferData(I2CDEV_M, &transferMCfg, I2C_TRANSFER_POLLING);
	if(DEBUG)
	{
		_DBG("\tI2C Transfer Started\n\r");
	}
	I2C1_IRQHandler();

	return true;
}

/**
 * Writes a string up to 16 characters to the LCD, starting at the
 * current position. If the current position is not the start of a line,
 * the write may move beyond the right of the LCD display.
 * @param  string The string to write to the LCD.
 * @param  length The length of the string to write to the LCD display. Best when < 17.
 * @return        True if successful.
 */
bool LCD_transmit_string(char* string, uint8_t length)
{
	int i;
	if (string[length-1] == '\0')
	{

		// debugMessageLength = sprintf(debugMessageBuffer, "Length = %d string[length-2] = %c\n\r", length, string[length-2]);
		// _DBG(debugMessageBuffer);

		uint8_t Transmit_Buffer[length + 1];
		Transmit_Buffer[0] = 0x44;	// Control Byte: Function Set: Write Data

		for (i = 0; i < length-1; ++i)
		{
			/*debugMessageLength = sprintf(debugMessageBuffer, "Char Number = 0x%x, Char = %c\n\r", i, string[i]);
			_DBG(debugMessageBuffer);*/
			Transmit_Buffer[i+1] = (string[i] + 0x80); // Data byte to transmit = ASCII code + 0x80
		}
		// debugMessageLength = sprintf(debugMessageBuffer, "\n\rSuccess = %d\n\r", sendDataLCD(Transmit_Buffer, length));
		// _DBG(debugMessageBuffer);
		sendDataI2C(Transmit_Buffer, length, LCD_TRANSFER, NULL, 0);
		return true; // Success
	}
	else
	{
		return false; // Incorrect string length
	}
}

/**
 * Moves the current position to the start of the line not currently selected.
 * @return True if successfully started
 */
bool LCD_transmit_newline()
{
	

	uint8_t Transmit_Buffer[1];

	// Read current address

	Transmit_Buffer[0] = 0x00;	// Control Byte: Function Set: Read Current Address
	g_LCDNewLineRecieveBuffer[0] = 0;

	sendDataI2C(Transmit_Buffer, 1, LCD_TRANSFER, g_LCDNewLineRecieveBuffer, 1);

	// Newline completed in interrupt service routine.

	return true; // Success
}

/**
* @desc Method 0 calls the screen clear LCD controller function, replacing the screen
* with *s. Method 1 writes two lines of ' ' to the LCD, effectively clearing the screen,
* before returning the current position to the start of the screen.
**/
bool LCD_screen_clear(int method)
{
	// Method 0 uses clear display command, which fills the screen with *s, method 1 fills the screen with " " charactors.
	
	uint8_t Transmit_Buffer[2];

	if (method == 0)
	{
		Transmit_Buffer[0] = 0x04;	// Control Byte: Function Set: Write Data

		Transmit_Buffer[1] = (0x01); // Clear display command
		
		sendDataI2C(Transmit_Buffer, 1, LCD_TRANSFER, NULL, 0);

		return true; // Success
	}
	else if (method == 1)
	{
		if(DEBUG)
		{
			_DBG("Running LCD_screen_clear using method 1\n\r");
		}
		// Call method 0 to return to start
		Transmit_Buffer[0] = 0x00;	// Control: Function Set
		Transmit_Buffer[1] = 0x80;	// Go to position 0
		
		sendDataI2C(Transmit_Buffer, 1, LCD_TRANSFER, NULL, 0);

		LCD_transmit_string("                ", 17);
		LCD_transmit_newline();
		LCD_transmit_string("                ", 17);
		LCD_transmit_newline();
		LCD_transmit_newline();
		return true; // Success
	}
	else
	{
		return false; // Incorrect parameter passed
	}
}

/**
 * Takes a column to check for key presses, and send i2c checking data for the column, which the interrupt handler deals with
 * @param  columnValues Values to check on each column
 * @return              True if successful.
 */
bool checkKeypad(uint8_t columnValues)
{
	// if(DEBUG)
	// {
	// 	_DBG("checkKeypad()\n\r");
	// }
	uint8_t Transmit_Buffer[1];

	Transmit_Buffer[0] = columnValues;

	// debugMessageLength = sprintf(debugMessageBuffer, "Data Sent: 0x%x\t", *Transmit_Buffer);
	// _DBG(debugMessageBuffer);
	//_DBG("Buffer:\n\r");

	// debugMessageLength = sprintf(debugMessageBuffer, "\t0x%x\n\r", Transmit_Buffer[0]);
	// _DBG(debugMessageBuffer);
	
	g_KeyPadTransferComplete = false;

	sendDataI2C(Transmit_Buffer, 1, KEYPAD_TRANSFER, g_KeypadRecieveBuffer, 1);

	return true;
}

/**
 * Enumerates the keypad codes to human-readable button labels.
 * @param  keyReceived 8 bit key code (rows selected and columns selected).
 * @return             equivalent value char to the key label of the button pressed.
 */
char keypadCharFromCode(char keyReceived)
{
	char out;
	switch(keyReceived)
	{
		case(0x77):
			out='1';
			break;
		case(0xb7):
			out='2';
			break;
		case(0xd7):
			out='3';
			break;
		case(0xe7):
			out='A';
			break;
		case(0x7b):
			out='4';
			break;
		case(0xbb):
			out='5';
			break;
		case(0xdb):
			out='6';
			break;
		case(0xeb):
			out='B';
			break;
		case(0x7d):
			out='7';
			break;
		case(0xbd):
			out='8';
			break;
		case(0xdd):
			out='9';
			break;
		case(0xed):
			out='C';
			break;
		case(0x7e):
			out='*';
			break;
		case(0xbe):
			out='0';
			break;
		case(0xde):
			out='#';
			break;
		case(0xee):
			out='D';
			break;
		default:
			out=' ';
			break;

	}
	return out;
}

/**
 * This method checks the keyboard, and returns the char pressed, provided only
 * one button at a time is pressed.
 * @return It returns 'X' if more than one button is pressed, and ' ' if no
 * buttons are pressed, otherwise it returns the label char of the pressed button.
 * This function checks the entire keypad over several calls for speed reasons.
 */
char getInput()
{
	static uint8_t columns[4]={0x7F,0xBF,0xDF,0xEF};

	if (g_KeyPadTransferComplete == true)
	{
		static int row = 0;
		checkKeypad(columns[row]);
		if (row <= 3)
		{
			row++;
		}
		else
		{
			row = 0;
		}

		return g_KeyPadInput;
	}
	else
	{
		return ' ';
	}
}