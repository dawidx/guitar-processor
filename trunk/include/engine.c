#include <math.h>
#include "engine.h"
//#include "../effects/filter.h"
#include "../effects/distortion.c"
#include "../effects/passive.c"
#include "../effects/echo.c"
#include "../effects/chorus.c"
#include "../effects/delay.c"
#include "../effects/high-pass.c"
#include "../effects/low-pass.c"


/**
* @global uint8_t g_MasterVolume holds the current volume applied to the final output
**/
uint8_t g_MasterVolume = 10;

/**
* @global uint32_t g_sampleRate holds the current sampling rate.
* @todo Implement procedure to reduce sampling rate if the system cannot cope with high frequency sampling.
**/
uint32_t g_sampleRate = MAX_SAMPLING_RATE;

/**
* @global uint16_8 g_AbailableMemory holds the available memory. Before each of the effect is generated. Memory is checked.
* @todo Add procedure to add to available memory if an effect is removed or the effects are reset.
* @todo Make sure the variable is always upto date
**/
uint16_t g_AvailableMemory = 30000;
uint8_t g_AvailableEffects[] = { 0, 1, 2, 3, 4 };

/**
* @desc the array with all the filters. Intitially, Reset_Filters() should be applied to set all the filters to passive ones
**/
Filter filters[MAX_PARALLEL][MAX_SERIES];

/**
* @desc Method applies appropiate effect depending on the given filter
* @param struct *filter is the filter
* @param uint8_t value is the value to be modified
**/
uint8_t Apply_Effect(Filter *filter, uint8_t value)
{
	switch (filter->ID)
	{
	case PASSIVE_EFFECT_ID:
		return Get_Passive_Value(value);
	case DISTORTION_EFFECT_ID:
		return Get_Distortion_Value(value, filter);
	case CHORUS_EFFECT_ID:
		return Get_Chorus_Value(value, g_sampleRate, filter);
	case ECHO_EFFECT_ID:
		return Get_Echo_Value(value, g_sampleRate, filter);
	case DELAY_EFFECT_ID:
		return Get_Delay_Value(value, g_sampleRate, filter);
	case HIGHPASS_EFFECT_ID:
		return Get_HighPass_Value(value, g_sampleRate, filter);
	case LOWPASS_EFFECT_ID:
		return Get_LowPass_Value(value, g_sampleRate, filter);
	default:
		return value;
	}
}


/**
* @desc Called for each sample, applys the effect table (which may combine multiple effects), preparing it for output.
**/
uint8_t Apply_Effects(uint8_t value)
{
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " Input Value %d \n\r ", (uint8_t) value);
		_DBG(debugMessageBuffer);
	}


	value = Apply_Noise_Gate(value);
	value = Apply_Spike_Gate(value);
	value = Apply_Compression(value);
	//Defining the array for each channel. The values stored represent value for each channel. Each channel gets the same input
	uint8_t channels[MAX_PARALLEL] = { value, value, value, value };
	//Defining the array of volumes for each channel. This will be later used for calculating output value (taking avarages etc.)
	uint16_t volumes[MAX_PARALLEL] = { 0, 0, 0, 0 };
	//Defining the array of effets that are active in each channel. An active effects is one where its volume is greater than 0 AND it is not proceeded by
	//any effect which volume is 0. 
	uint8_t active[MAX_PARALLEL] = { 0, 0, 0, 0 };
	//Enumerating through each channel in series
	uint16_t series, parallel, new_value = 0;
	//foreach channel
	for (parallel = 0; parallel < MAX_PARALLEL; parallel++)
	{
		//Apply effects in series to produce the output
		//the loop will break if volume of the next filter is 0
		//the value so far, will be preserved
		for (series = 0; series < MAX_SERIES; series++)
		{
			//if volume is 0 no point going through the rest, becuase the input singal is 0. However, the current value in channels[parallel] is preserved
			if (Get_Effect_Volume(&filters[parallel][series]) == 0) {
				break;
			} else {
				//the elements are in series so we feed the previous value with relevant filter and multiply it by the value
				volumes[parallel] += Get_Effect_Volume(&filters[parallel][series]);
				active[parallel]++;
				if(DEBUG) {
					debugMessageLength = sprintf(debugMessageBuffer, " Before applying effect Channel: %d, Effect: %d Value: %d \n\r ", parallel, series, channels[parallel]);
					_DBG(debugMessageBuffer);
				}
				//Applying the effect to the given amplitude
				//We also apply the volume. By default, all active effects have maximum volume (hence you can only turn them down)
				channels[parallel] = (float)Apply_Effect(&filters[parallel][series], channels[parallel]) * ((float)Get_Effect_Volume(&filters[parallel][series]) / 100.0);
				if(DEBUG) {
					debugMessageLength = sprintf(debugMessageBuffer, " After applying effect Channel: %d, Effect: %d Value: %d \n\r ", parallel, series, channels[parallel]);
					_DBG(debugMessageBuffer);
				}
			}
		}

		//taking the average volume for the channel. The aveage is based on the volumes foreach effect
		//We need to be careful, if there are no active effects in a given channel we would divide by 0
		if(active[parallel] > 0) {
			volumes[parallel] = volumes[parallel] / active[parallel];
			if(DEBUG) {
				debugMessageLength = sprintf(debugMessageBuffer, " Average Volume for a channel: %d \n\r ", volumes[parallel]);
				_DBG(debugMessageBuffer);
			}
		}
		//else the volumes are set to 0 anyway

		//Now each channel is mulitplied by its relative volume
		//The Worst case scenario:
		//4 effects in series have value of 100 (100+100+100+100 = 400)
		//They are summed up and divided (by 4) above to give the value of 100
		//Now we have a cofficient k | 0 <= k <= 1
		float k = (float)volumes[parallel] / 100.0;
		channels[parallel] = (float)channels[parallel] * k;
		//volumes[parallel] = volumes[parallel] / (active[parallel] * 100);
		if(DEBUG) {
			debugMessageLength = sprintf(debugMessageBuffer, " Final Output form channel: %d \n\r ", channels[parallel]);
			_DBG(debugMessageBuffer);
		}
	}

	//An active channel is a channel that has at least one active effect (volume > 0)
	uint8_t activechannels = 0;
	uint8_t k;
	for(k = 0; k < MAX_PARALLEL; k++) {
		//If there is an active effect
		if(active[k] > 0) {
			activechannels++;
			//Adding the value to the new value
			//Average will be taken later once we know how many active channels we have.
			new_value += channels[k];
			if(DEBUG) {
				debugMessageLength = sprintf(debugMessageBuffer, " Loop: %d Value: %d \n\r ", k, new_value);
				_DBG(debugMessageBuffer);
			}
		}
	}
	//The final value is the average of the output of all active channels.
	new_value = new_value / activechannels;
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " New value avarage: %d \n\r ", new_value);
		_DBG(debugMessageBuffer);
	}

	/*
		@todo work on this algorithm. It is supposed to use 8bit value from the effets and use the next 2 bits for master volume.
		If the value
	*/
	new_value = (float)Get_Volume() / 10.0 * (float)new_value;
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " Final Value (16bits): %d \n\r ", new_value);
		_DBG(debugMessageBuffer);
	}
	return new_value;
}

void Reset_Filters()
{
	//We rest all the filters to passive ones with volume 0
	//In Engine V3 we all filters with volume 0 are ignored
	int a, b;
	for (a = 0; a < MAX_PARALLEL; a++) {
		for (b = 0; b < MAX_SERIES; b++) {
			filters[a][b].ID = 0;
			Set_Effect_Volume(0, &filters[a][b]);
		}
	}
	//The first filter is the passive one (with voluem 100)
	//SetEffect(0,0,1);
	SetEffect(1,0,4);
}

bool Set_Effect_Volume(uint8_t volume, Filter *filter) {
	if (volume <= 100 && volume >= 0) {
		filter->Parameter[0] = volume;
		return true;
	}
	else {
		return false;
	}
}

uint8_t Get_Effect_Volume(Filter *filter) {
	if(DEBUG) {

		debugMessageLength = sprintf(debugMessageBuffer, " Volume: %d \n\r ", filter->Parameter[0]);
		_DBG(debugMessageBuffer);
	}
	return filter->Parameter[0];
}

uint8_t Get_Volume() {
	return g_MasterVolume;
}

bool IncreaseVolume(uint8_t currentVolume) {
	if (currentVolume <= 20) {
		SetVolume(currentVolume + 1);
		return true;
	}
	else {
		return false;
	}
}

bool DecreaseVolume(uint8_t currentVolume) {
	if (currentVolume >= 0) {
		SetVolume(currentVolume - 1);
		return true;
	}
	else {
		return false;
	}
}

bool SetVolume(uint8_t volume) {
	if (volume <= 40 && volume >= 0) {
		g_MasterVolume = volume;
		return true;
	}
	else {
		return false;
	}
}

char* Get_EffectName(uint8_t effectID) {
	switch (effectID) {
	case PASSIVE_EFFECT_ID:
		return "<Passive       >";
	case DISTORTION_EFFECT_ID:
		return "<Distortion    >";
	case CHORUS_EFFECT_ID:
		return "<Chorus        >";
	case ECHO_EFFECT_ID:
		return "<Echo          >";
	case DELAY_EFFECT_ID:
		return "<Delay         >";
	case LOWPASS_EFFECT_ID:
		return "<Low Pass      >";
	case HIGHPASS_EFFECT_ID:
		return "<High Pass     >";
	default:
		return "Error           ";
	}
}

uint8_t Get_EffectID(uint8_t channel, uint8_t location) {
	return filters[channel][location].ID;
}

uint8_t* Get_AllAvailableEffects(uint8_t channel, uint8_t location) {
	return g_AvailableEffects;
}

bool ResetEffectDefaults(uint8_t channel, uint8_t location) {
	uint8_t id = filters[channel][location].ID;
	//Freeing up the memory space
	_DBG("About to free...\n\r");
	//free(filters[channel][location].Buffer);
	_DBG("...Freed\n\r");
	SetEffect(channel, location, id);
	return true;
}

bool ClearEffect(uint8_t channel, uint8_t location) {
	//freeing up memory
	//free(filters[channel][location].Buffer);
	//set it to passive
	SetEffect(channel, location, 0);
	return true;
}


bool SetEffect(uint8_t channel, uint8_t location, uint8_t effectID) {
	char debugMessageBuffer[0xFF];
	int debugMessageLength;

	debugMessageLength = sprintf(debugMessageBuffer, "SetEffect:189\n\r\tCalled SetEffect with variables:"
		"\n\r\t\tchannel: %d\n\r\t\tlocation: %d\n\r\t\teffectID: %d\n\r",channel, location, effectID);
	_DBG(debugMessageBuffer);
	//Cheching the memory
	if(!Check_Available_Memory(effectID)) {
		return false;
	}
	switch (effectID)
	{
		case PASSIVE_EFFECT_ID:
			filters[channel][location] = Generate_Passive_Effect();
			return true;
		case DISTORTION_EFFECT_ID:
			filters[channel][location] = Generate_Distortion_Effect();
			return true; 
		case CHORUS_EFFECT_ID:
			filters[channel][location] = Generate_Chorus_Effect();
			// _DBG("SetEffect:202\n\r\tReturned\n\r");
			return true;
		case ECHO_EFFECT_ID:
			filters[channel][location] = Generate_Echo_Effect();
			return true;
		case DELAY_EFFECT_ID:
			filters[channel][location] = Generate_Delay_Effect();
			return true;
		case LOWPASS_EFFECT_ID:
			filters[channel][location] = Generate_LowPass_Effect();
			return true;
		case HIGHPASS_EFFECT_ID:
			filters[channel][location] = Generate_HighPass_Effect();
			return true;
		default:
			filters[channel][location] = Generate_Passive_Effect();
			return false;
	}
}

uint8_t Get_NumberEffectVariables(uint8_t effectID) {
	switch (effectID){
	case PASSIVE_EFFECT_ID:
		return 1;
	case DISTORTION_EFFECT_ID:
		return 2;
	case CHORUS_EFFECT_ID:
		return 5;
	case ECHO_EFFECT_ID:
		return 3;
	case DELAY_EFFECT_ID:
		return 3;
	case LOWPASS_EFFECT_ID:
		return 2;
	case HIGHPASS_EFFECT_ID:
		return 2;
	default:
		return 0;
	}
}

char* Get_EffectVariableName(uint8_t effectID, uint8_t variable) {
	if (variable == 0) {
		return "Volume       ";
	}
	else {
		switch (effectID){
		case DISTORTION_EFFECT_ID:
			return "Threshold       ";
		case CHORUS_EFFECT_ID:
			if (variable == 1) {
				return "Delay           ";
			}
			else if (variable == 2) {
				return "Volume Guitar 1 ";
			}
			else if (variable == 3) {
				return "Volume Guitar 2 ";
			}
			if (variable == 4) {
				return "Variant Level   ";
			}
			else {
				return "Error           ";
			}
			break;
		case ECHO_EFFECT_ID:
			if (variable == 1) {
				return "Strength        ";
			}
			else if (variable == 2) {
				return "Delay           ";
			} else {
				return "Error           ";
			}
			break;
		case DELAY_EFFECT_ID:
			if (variable == 1) {
				return "Strength        ";
			}
			else if (variable == 2) {
				return "Delay           ";
			}
			else {
				return "Error           ";
			}
		case LOWPASS_EFFECT_ID:
			if (variable == 1) {
				return "Breakpoint Freq ";
			}
			else {
				return "Error           ";
			}
		case HIGHPASS_EFFECT_ID:
			if (variable == 1) {
				return "Breakpoint Freq ";
			}
			else {
				return "Error           ";
			}
		default:
			return "Error           ";
			break;
		}
	}
}

uint8_t Get_EffectVariableCurrentValue(uint8_t channel, uint8_t location, uint8_t variableNumber)
{
	return filters[channel][location].Parameter[variableNumber];
}


bool IncrementVariableCurrentValue(uint8_t channel, uint8_t location, uint8_t variableNumber)
{
	if (variableNumber == 0)
	{
		Set_Effect_Volume(Get_Effect_Volume(&filters[channel][location]) + 5, &filters[channel][location]);
		return true;
	}
	else {
		switch (filters[channel][location].ID){
		case DISTORTION_EFFECT_ID:
			Set_Distortion_Threshold(Get_Distortion_Threshold(&filters[channel][location]) + 1, &filters[channel][location]);
			return true;
		case CHORUS_EFFECT_ID:
			if (variableNumber == 1) {
				Set_Chorus_Time_Value(Get_Chorus_Time_Value(&filters[channel][location]) + 5, &filters[channel][location]);
				return true;
			}
			else if (variableNumber == 2) {
				Set_Chorus_Voice1_Volume(Get_Chorus_Voice1_Volume(&filters[channel][location]) + 5, &filters[channel][location]);
				return true;
			}
			else if (variableNumber == 3) {
				Set_Chorus_Voice2_Volume(Get_Chorus_Voice2_Volume(&filters[channel][location]) + 5, &filters[channel][location]);
				return true;
			}
			else if (variableNumber == 4) {
				Set_Chorus_Time_Variant(Get_Chorus_Time_Variant(&filters[channel][location]) + 1, &filters[channel][location]);
				return true;
			}
			else {
				return false;
			}
		case ECHO_EFFECT_ID:
			if (variableNumber == 1) {
				Set_Echo_Strength(Get_Echo_Strength(&filters[channel][location]) + 5, &filters[channel][location]);
				return true;
			}
			else if (variableNumber == 2) {
				Set_Echo_Time(Get_Echo_Time(&filters[channel][location]) + 5, &filters[channel][location]);
				return true;
			}
			else {
				return false;
			}
		case DELAY_EFFECT_ID:
			if (variableNumber == 1) {
				Set_Delay_Strength(Get_Delay_Strength(&filters[channel][location]) + 5, &filters[channel][location]);
				return true;
			}
			else if (variableNumber == 2) {
				Set_Delay_Time(Get_Delay_Time(&filters[channel][location]) + 5, &filters[channel][location]);
				return true;
			}
			else {
				return false;
			}
		case LOWPASS_EFFECT_ID:
			if (variableNumber == 1) {
				Set_LowPass_Breakpoint_Frequency(Get_LowPass_Breakpoint_Frequency(&filters[channel][location]) * 2, &filters[channel][location]);
				return true;
			}
			else {
				return false;
			}
		case HIGHPASS_EFFECT_ID:
			if (variableNumber == 1) {
				Set_HighPass_Breakpoint_Frequency(Get_HighPass_Breakpoint_Frequency(&filters[channel][location]) * 2, &filters[channel][location]);
				return true;
			}
			else {
				return false;
			}
		}
	}
	return true;
}



bool SetVariableCurrentValue(uint8_t channel, uint8_t location, uint8_t variableNumber, uint8_t value)
{
	if (variableNumber == 0)
	{
		Set_Effect_Volume(value, &filters[channel][location]);
		return true;
	}
	else {
		switch (filters[channel][location].ID){
		case DISTORTION_EFFECT_ID:
			Set_Distortion_Threshold(value, &filters[channel][location]);
			return true;
		case CHORUS_EFFECT_ID:
			if (variableNumber == 1) {
				Set_Chorus_Time_Value(value, &filters[channel][location]);
				return true;
			}
			else if (variableNumber == 2) {
				Set_Chorus_Voice1_Volume(value, &filters[channel][location]);
				return true;
			}
			else if (variableNumber == 3) {
				Set_Chorus_Voice2_Volume(value, &filters[channel][location]);
				return true;
			}
			else if (variableNumber == 4) {
				Set_Chorus_Time_Variant(value, &filters[channel][location]);
				return true;
			}
			else {
				return false;
			}
		case ECHO_EFFECT_ID:
			if (variableNumber == 1) {
				Set_Echo_Strength(value, &filters[channel][location]);
				return true;
			}
			else if (variableNumber == 2) {
				Set_Echo_Time(value, &filters[channel][location]);
				return true;
			}
			else {
				return false;
			}
		case DELAY_EFFECT_ID:
			if (variableNumber == 1) {
				Set_Delay_Strength(value, &filters[channel][location]);
				return true;
			}
			else if (variableNumber == 2) {
				Set_Delay_Time(value, &filters[channel][location]);
				return true;
			}
			else {
				return false;
			}
		case LOWPASS_EFFECT_ID:
			if (variableNumber == 1) {
				Set_LowPass_Breakpoint_Frequency(value, &filters[channel][location]);
				return true;
			}
			else {
				return false;
			}
		case HIGHPASS_EFFECT_ID:
			if (variableNumber == 1) {
				Set_HighPass_Breakpoint_Frequency(value, &filters[channel][location]);
				return true;
			}
			else {
				return false;
			}
		}
	}
	return true;
}


bool DecrementVariableCurrentValue(uint8_t channel, uint8_t location, uint8_t variableNumber) {

	if (variableNumber == 0) {
		Set_Effect_Volume(Get_Effect_Volume(&filters[channel][location]) - 5, &filters[channel][location]);
		return true;
	}
	else {
		switch (filters[channel][location].ID){
		case DISTORTION_EFFECT_ID:
			Set_Distortion_Threshold(Get_Distortion_Threshold(&filters[channel][location]) - 1, &filters[channel][location]);
			return true;
		case CHORUS_EFFECT_ID:
			if (variableNumber == 1) {
				Set_Chorus_Time_Value(Get_Chorus_Time_Value(&filters[channel][location]) - 5, &filters[channel][location]);
				return true;
			}
			else if (variableNumber == 2) {
				Set_Chorus_Voice1_Volume(Get_Chorus_Voice1_Volume(&filters[channel][location]) - 5, &filters[channel][location]);
				return true;
			}
			else if (variableNumber == 3) {
				Set_Chorus_Voice2_Volume(Get_Chorus_Voice2_Volume(&filters[channel][location]) - 5, &filters[channel][location]);
				return true;
			}

			else if (variableNumber == 3) {
				Set_Chorus_Time_Variant(Get_Chorus_Voice2_Volume(&filters[channel][location]) - 1, &filters[channel][location]);
				return true;
			}
			else {
				return false;
			}
		case ECHO_EFFECT_ID:
			if (variableNumber == 1) {
				Set_Echo_Strength(Get_Echo_Strength(&filters[channel][location]) - 5, &filters[channel][location]);
				return true;
			}
			else if (variableNumber == 2) {
				Set_Echo_Time(Get_Echo_Time(&filters[channel][location]) - 5, &filters[channel][location]);
				return true;
			}
			else {
				return false;
			}
		case DELAY_EFFECT_ID:
			if (variableNumber == 1) {
				Set_Delay_Strength(Get_Delay_Strength(&filters[channel][location]) - 5, &filters[channel][location]);
				return true;
			}
			else if (variableNumber == 2) {
				Set_Delay_Time(Get_Delay_Time(&filters[channel][location]) - 5, &filters[channel][location]);
				return true;
			}
			else {
				return false;
			}
		case LOWPASS_EFFECT_ID:
			if (variableNumber == 1) {
				Set_LowPass_Breakpoint_Frequency(Get_LowPass_Breakpoint_Frequency(&filters[channel][location]) / 2, &filters[channel][location]);
				return true;
			}else {
				return false;
			}
		case HIGHPASS_EFFECT_ID:
			if (variableNumber == 1) {
				Set_HighPass_Breakpoint_Frequency(Get_HighPass_Breakpoint_Frequency(&filters[channel][location]) / 2, &filters[channel][location]);
				return true;
			}else {
				return false;
			}
		}
	}
	return true;
}


uint16_t Get_Available_Memory() {
	return g_AvailableMemory;
}

bool Check_Available_Memory(uint8_t effectID) {
	switch (effectID) {
	case PASSIVE_EFFECT_ID:
		//Small number, because passive filter doesn't use a lot of memory
		if (Get_Available_Memory() > 1) {
			//Memory set;
			return true;
		}
		else {
			//Memory full
			return false;
		}
		break;
	case DISTORTION_EFFECT_ID:
		//Small number, because distortion filter doesn't use a lot of memory
		if (Get_Available_Memory() > 1) {
			//Memory set;
			return true;
		}
		else {
			//Memory full
			return false;
		}
		break;
	case CHORUS_EFFECT_ID:
		//Chorus needs about 80 ms at 44.1 kHz ~= 4000 memory locations
		if (Get_Available_Memory() > 4000) {
			//Memory set;
			return true;
		}
		else {
			//Memory full
			return false;
		}
		break;
	case ECHO_EFFECT_ID:
		//Echo needs about 500 ms at 44.1 kHz ~= 23000 memory locations
		if (Get_Available_Memory() > 23000) {
			//Memory set;
			return true;
		}
		else {
			//Memory full
			return false;
		}
		break;
	case DELAY_EFFECT_ID:
		//Delay needs about 500 ms at 44.1 kHz ~= 23000 memory locations
		if (Get_Available_Memory() > 23000) {
			//Memory set;
			return true;
		}
		else {
			//Memory full
			return false;
		}
		break;
	case LOWPASS_EFFECT_ID:
		//Both filters need about 100 locations
		if (Get_Available_Memory() > 100) {
			//Memory set;
			return true;
		}
		else {
			//Memory full
			return false;
		}
		break;
	case HIGHPASS_EFFECT_ID:
		//Both filters need about 100 locations
		if (Get_Available_Memory() > 100) {
			//Memory set;
			return true;
		}
		else {
			//Memory full
			return false;
		}
		break;
	default:
		return false;
	}
}

uint8_t Get_NumEffects(uint8_t channel, uint8_t location) {
	return 5;// sizeof(g_AvailableEffects) / sizeof(g_AvailableEffects[0]);
}

/**
Threshold around centre (128)
**/
uint8_t Apply_Noise_Gate(uint8_t value) {
	static int mean = 0;
	mean = (mean + value) >> 1;

	//e.g. mean is 158
	//then the threshold is 30 * 15% = 5
	int threshold = ((mean > 128) ? (mean - 128) : mean) * (float)NOISE_GATE_PERCENT / 100.0;

	//if the value is greater than the threshold in both directions
	if(value > (128+threshold) || (value < (128-threshold))) {
		return value;
	} else {
		return 128; //effectively 0;
	}
}

uint8_t Apply_Spike_Gate(uint8_t value) {
	//Storing last 10 input values
	static int last[10] = {0,0,0,0,0,0,0,0,0,0};
	static int index = 0;
	int mean = 0;
	int i;
	for(i = 0; i < 10; i++) {
		mean += last[i];
	}

	mean = mean / 10;
	if(mean > 128) {
		mean = mean - 128;
	}
	int value_to_return = value;
	//if mean is 30
	//then the threshold is 30 * 30% = 9
	//Spikes usually manifest them with maximum / almost maximum value. Hence even a small threshold can put a stop
	int threshold = (mean * SPIKE_GATE_PERCENT)/100;
	if (value > (255-threshold) && value < threshold) {
		//return the last one that satisfies the inequality
		int k;
		for (k = 9; k >= 0; k--) {
			if (last[k] < (255-threshold) || last[k] > threshold) {
				value_to_return = last[k];
			} 
		}
	}
	last[index] = value;
	if(index == 9) {
		index = 0;
	} else {
		index++;
	}
	return value_to_return;
}
/**
* @todo research compression algrithms and implement one;
**/
uint8_t Apply_Compression(uint8_t value) {
	//The signal that actually passes through the Noise gate then gets amplified
	return value;

}