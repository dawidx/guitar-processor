/**
* @file contains an implementation of the echo effect
**/
#include "echo.h"

uint8_t Get_Echo_Value(uint8_t value, uint16_t samplingRate, Filter *filter) {
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " Input Value: %d \n\r ", value);
		_DBG(debugMessageBuffer);
	}

	uint16_t off = Get_Echo_Time(filter) * 10 * samplingRate / 1000;
	
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " Expected Offset: %d \n\r ", off);
		_DBG(debugMessageBuffer);
	}
	//Firstly, we retrive the offset of previously stored value. This value depends on the sampling rate and the echo time
	uint16_t offset = Get_Echo_Buffer_Offset(off, off, filter);

	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " Offset Value: %d \n\r ", offset);
		_DBG(debugMessageBuffer);
	}

	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " Value in the buffer: %d \n\r ", filter->Buffer[offset]);
		_DBG(debugMessageBuffer);
	}
	//we calculate the new value but considering the strength of the echo
	//uint8_t new_value = ((100 - Get_Echo_Strength(filter)) * value)/100 + (Get_Echo_Strength(filter) * filter->Buffer[offset])/100;

	uint8_t new_value = ((100 - Get_Echo_Strength(filter)) * value)/100 + (Get_Echo_Strength(filter) * filter->Buffer[offset])/100;
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " New Value: %d \n\r ", new_value);
		_DBG(debugMessageBuffer);
	}
	//Storing the value for future use
	filter->Buffer[filter->Index] = new_value;
	if(DEBUG) {
		_DBG("Value stored in the buffer\n\r");
		debugMessageLength = sprintf(debugMessageBuffer, " Value in the buffer: %d \n\r ", filter->Buffer[filter->Index]);
		_DBG(debugMessageBuffer);
	}
	//Increasing the filter offset
	filter->Index = Get_Echo_Buffer_Offset(1, off, filter);
	return new_value;
}

Filter Generate_Echo_Effect() {
	Filter filter;
	filter.ID = ECHO_EFFECT_ID;
	filter.Index = 0;
	Set_Effect_Volume(100, &filter);
	Set_Echo_Strength(70, &filter);
	//In miliseconds
	Set_Echo_Time(49, &filter);
	uint8_t array[22050];
	filter.Buffer = array;
	return filter;
}

bool Set_Echo_Strength(uint8_t value, Filter *filter) {
	if (value <= 100) {
		filter->Parameter[1] = value;
		return true;
	}
	else {
		return false;
	}
}

uint8_t Get_Echo_Strength(Filter *filter) {
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " Echo Strength: %d \n\r ", filter->Parameter[1]);
		_DBG(debugMessageBuffer);
	}
	return filter->Parameter[1];
}

bool Set_Echo_Time(uint8_t value, Filter *filter) {
	if (value <= 50) {
		filter->Parameter[2] = value;
		return true;
	}
	else {
		return false;
	}
}

uint8_t Get_Echo_Time(Filter *filter) {
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " Echo Time: %d \n\r ", filter->Parameter[2]);
		_DBG(debugMessageBuffer);
	}
	return filter->Parameter[2];
}


/**
* @desc The function calculates and returns the memory location with given offset. Note: The buffer stores from BUFFERSIZE to 0, hence the location is g_InputBufferStart PLUS the offset
* @param uint16_t offset the number of locations before the current sample
**/
uint16_t Get_Echo_Buffer_Offset(uint16_t offset, int16_t max2, Filter *filter) {
	
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " Max: %d \n\r ", max2);
		_DBG(debugMessageBuffer);
	}
	if (offset <= max2) {
		uint16_t location = filter->Index + offset;
		if (location >= max2) {
			location = location - max2;
		}
		return location;
	}
	_DBG("Error");
	//Error_Report("Overflow error", 4);
	return 0;
}