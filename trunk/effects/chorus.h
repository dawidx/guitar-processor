uint8_t Generate_Chorus_Value(uint8_t value, uint16_t samplingRate, Filter *filter);
Filter Generate_Chorus_Effect();
uint16_t Generate_Chorus_Offset(Filter *filter, uint16_t samplingRate);
uint16_t Get_Chorus_Index_Offset(uint16_t offset, uint16_t max, Filter *filter);
bool Set_Chorus_Time_Value(uint8_t value, Filter *filter);
uint8_t Get_Chorus_Time_Value(Filter *filter);
uint8_t Get_Chorus_Voice1_Volume(Filter *filter);
uint8_t Get_Chorus_Voice2_Volume(Filter *filter);
bool Set_Chorus_Voice1_Volume(uint8_t value, Filter *filter);
bool Set_Chorus_Voice2_Volume(uint8_t value, Filter *filter);
bool Set_Chorus_Time_Variant(uint8_t value, Filter *filter);
uint8_t Get_Chorus_Time_Variant(Filter *filter);
float Get_Chorus_Angle(Filter *filter);