#include "high-pass.h"

/**
* @desc Implementation of a recursive high pass filter based on the wiki article
* @see http://en.wikipedia.org/wiki/High-pass_filter
* @param uint8_t value the sample
* @param uint16_t samplingRate the samplingRate is used on only to determine the offset but also to determine the steepness of the roll off
* @param Filter *filter the effect
**/
uint8_t Get_HighPass_Value(uint8_t value, uint16_t samplingRate, Filter *filter) {
	//Simple recursive low-pass filter
	//Storing the value in the buffer for the future reference
	filter->Buffer[filter->Index] = value;
	float dt = 1.0 / (float)samplingRate;
	//Standard equation for calculating breakpoint frequency RC = 1/(2*PI*Fb) where Fb is the breakpoint frequency
	float RC = 1.0 / (2.0 * (float)PI * (float)Get_HighPass_Breakpoint_Frequency(filter));
	float alpha = RC / (RC + dt);
	//Exponentially moving average. First order differential equation
	float new_value[20];
	new_value[0] = (float)filter->Buffer[Get_HighPass_Index_Offset(1, filter)];
	//Analysing the last 20 values. to determine the slope
	int i;
	for (i = 1; i < 20; i++) {
		new_value[i] = alpha * ((float)new_value[i - 1] + (float)filter->Buffer[Get_HighPass_Index_Offset(i+1, filter)] - (float)filter->Buffer[Get_HighPass_Index_Offset(i, filter)]);
	}
	//Increment index
	filter->Index = Get_HighPass_Index_Offset(1, filter);
	//returning the last value
	return (uint8_t) new_value[19];
}

/**
* @desc The method returns the offset 
**/
uint16_t Get_HighPass_Index_Offset(uint16_t offset, Filter *filter) {
	if ((offset + filter->Index) >= 20) {
		return filter->Index - 20;
	}
	else {
		return filter->Index + offset;
	}
}
/**
* @desc Generates new Highpass Effect
* @return Filter filter 
**/
Filter Generate_HighPass_Effect() {
	Filter filter;
	uint8_t array[20];
	filter.Buffer = array;
	filter.Index = 0;
	Set_Effect_Volume(100, &filter);
	Set_HighPass_Breakpoint_Frequency(2000, &filter);
	return filter;
}
/**
* @desc Sets the Brakepoint frequency
**/
bool Set_HighPass_Breakpoint_Frequency(uint16_t frequency, Filter *filter) {
	if (frequency > 100 && frequency <= 22050) {
		filter->Parameter[1] = frequency;
		return true;
	}
	return false;
}

uint16_t Get_HighPass_Breakpoint_Frequency(Filter *filter) {
	return filter->Parameter[1];
}