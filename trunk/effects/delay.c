#include "delay.h"

uint8_t Get_Delay_Value(uint8_t value, uint16_t samplingRate, Filter *filter) {

	filter->Buffer[filter->Index] = value;

	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " Input Value: %d \n\r ", value);
		_DBG(debugMessageBuffer);
	}
	uint16_t off = Get_Delay_Time(filter) * 10 * samplingRate / 1000;
	//Firstly, we retrive the offset of previously stored value. This value depends on the sampling rate and the echo time
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " Expected Offset: %d \n\r ", off);
		_DBG(debugMessageBuffer);
	}
	
	uint16_t offset = Get_Delay_Buffer_Offset(off, off, filter);
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " Offset Value: %d \n\r ", offset);
		_DBG(debugMessageBuffer);
	}

	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " Value in the buffer: %d \n\r ", filter->Buffer[offset]);
		_DBG(debugMessageBuffer);
	}
	//we calculate the new value but considering the strength of the echo
	//uint8_t new_value = ((100 - Get_Delay_Strength(filter)) * value)/100 + (Get_Delay_Strength(filter) * filter->Buffer[offset])/100;

	uint8_t new_value = ((100 - Get_Delay_Strength(filter)) * value)/100 + (Get_Delay_Strength(filter) * filter->Buffer[offset])/100;
	if(DEBUG) {
		debugMessageLength = sprintf(debugMessageBuffer, " New Value: %d \n\r ", new_value);
		_DBG(debugMessageBuffer);
	}
	
	if(DEBUG) {
		_DBG("Value stored in the buffer\n\r");
		debugMessageLength = sprintf(debugMessageBuffer, " Value in the buffer: %d \n\r ", filter->Buffer[filter->Index]);
		_DBG(debugMessageBuffer);
	}
	//Increasing the filter offset
	filter->Index = Get_Delay_Buffer_Offset(1, off, filter);
	return new_value;
}

Filter Generate_Delay_Effect() {
	Filter filter;
	//Assigning ID
	filter.ID = DELAY_EFFECT_ID;
	//Setting the memory index to 0
	filter.Index = 0;
	//Setting the volume to 100
	Set_Effect_Volume(100, &filter);
	//Setting the strenth of the delay to be greater than the original signal 70:30
	Set_Delay_Strength(70, &filter);
	//In centiseconds
	Set_Delay_Time(49, &filter);

	//The memory
	uint8_t array[22050];
	filter.Buffer = array;
	return filter;
}
/**
* 
**/
bool Set_Delay_Strength(uint8_t value, Filter *filter) {
	if (value <= 100) {
		filter->Parameter[1] = value;
		return true;
	}
	else {
		return false;
	}
}

/**
* @desc Retrives the strength of the delay effect
* @param Filter *filter pointer to the delay effect
* @return uint8_t strength of the delay
**/
uint8_t Get_Delay_Strength(Filter *filter) {
	return filter->Parameter[1];
}

/**
* @desc Retrives the time delay for the delay effect
* @param Filter *filter Pointer to the delay effect
* @param uint8_t value the value in the form of CENTIseconds (x10^-2)
* @return Bool true; if successful set.
**/
bool Set_Delay_Time(uint8_t value, Filter *filter) {
	if (value <= 50) {
		filter->Parameter[2] = value;
		return true;
	}
	else {
		return false;
	}
}

/**
* @desc Retrives the time delay for the delay effect
* @param Filter Pointer to the delay effect
* @return Time delay. The value has format of CENTIseconds
**/
uint8_t Get_Delay_Time(Filter *filter) {
	return filter->Parameter[2];
}


/**
* @desc The function calculates and returns the memory location with given offset. 
* @param uint16_t offset the number of locations before the current sample
* @param uint16_t max the maximum number of locations
* @param Filter *filter pointer to the filter to get the current index
* @return uint16_t Offset. Returns 0 error occured (essensialy a passive effect)
**/
uint16_t Get_Delay_Buffer_Offset(uint16_t offset, uint16_t max, Filter *filter) {
	if (offset <= max) {
		uint16_t location = filter->Index + offset + 1;
		if (location >= max) {
			location = location - max;
		}
		return location;
	}
	_DBG("Error");
	//Error_Report("Overflow error", 4);
	return 0;
}