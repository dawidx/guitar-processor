uint8_t Get_HighPass_Value(uint8_t value, uint16_t samplingRate, Filter *filter);
uint16_t Get_HighPass_Index_Offset(uint16_t offset, Filter *filter);
Filter Generate_HighPass_Effect();
bool Set_HighPass_Breakpoint_Frequency(uint16_t frequency, Filter *filter);
uint16_t Get_HighPass_Breakpoint_Frequency(Filter *filter);
