#include "low-pass.h"

uint8_t Get_LowPass_Value(uint8_t value, uint16_t samplingRate, Filter *filter) {
	//Simple recursive low-pass filter
	//Storing the value in the buffer for the future reference
	filter->Buffer[filter->Index] = value;
	float dt = 1.0 / (float)samplingRate;
	//Standard equation for calculating breakpoint frequency RC = 1/(2*PI*Fb) where Fb is the breakpoint frequency
	float RC = 1.0 / (2.0 * (float)PI * (float)Get_LowPass_Breakpoint_Frequency(filter));
	float alpha = dt / (RC + dt);
	//Exponentially moving average. First order differential equation
	float new_value[20];
	new_value[0] = (float)filter->Buffer[Get_LowPass_Index_Offset(1, filter)];
	int i;
	for (i = 1; i < 20; i++) {
		new_value[i] = new_value[i - 1] + alpha * ((float)filter->Buffer[Get_LowPass_Index_Offset(i, filter)] - new_value[i - 1]);
	}
	//Increment index
	filter->Index = Get_LowPass_Index_Offset(1, filter);
	//returning the last value
	return (uint8_t) new_value[19];
}

uint16_t Get_LowPass_Index_Offset(uint16_t offset, Filter *filter) {
	if (offset <= 20) 
	{
		uint8_t location = filter->Index+offset;
		if(location >= 20) {
			return location - 20;
		} else {
			return location;
		}
	} else {
		_DBG("Error");
		return 0;
	}
}

Filter Generate_LowPass_Effect() {
	Filter filter;
	uint8_t array[20];
	filter.ID = LOWPASS_EFFECT_ID;
	filter.Buffer = array;
	filter.Index = 0;
	Set_Effect_Volume(100, &filter);
	Set_LowPass_Breakpoint_Frequency(500, &filter);
	return filter;
}

bool Set_LowPass_Breakpoint_Frequency(uint16_t frequency, Filter *filter) {
	if (frequency > 100 && frequency <= 22050) {
		filter->Parameter[1] = frequency;
		return true;
	}
	return false;
}

uint16_t Get_LowPass_Breakpoint_Frequency(Filter *filter) {
	return filter->Parameter[1];
}