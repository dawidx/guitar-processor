// Serial code
#include "lpc17xx_uart.h"		// Central include files
#include "lpc17xx_pinsel.h"
#include "lpc_types.h"
#include "lpc17xx_i2c.h"
#include "KeypadInt.h"			// Local functions
#include "debug_frmwrk.h"		// Debug messages
#include <stdio.h>

#define I2CDEV_S_ADDR_LCD	(0x3B)	// LCD Display Controller address
#define I2CDEV_S_ADDR_KEYPAD (0x21) // Keypad interface address
#define I2CDEV_M LPC_I2C1		// I2C Interface

__IO FlagStatus complete;

void I2C1_IRQHandler(void)
{
    // just call std int handler
    // _DBG("Interrupt\n\r");
    I2C_MasterHandler(I2CDEV_M);
    if (I2C_MasterTransferComplete(I2CDEV_M))
    {
    	// _DBG("\tTransferComplete");
        complete = SET;
    }
}

void i2c_init(void)
{

    /* Disable I2C2 interrupt */
    NVIC_DisableIRQ(I2C1_IRQn);
    /* preemption = 1, sub-priority = 1 */
    NVIC_SetPriority(I2C1_IRQn, ((0x01<<3)|0x01));

	PINSEL_CFG_Type PinCfg; // Pin configuration
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;

	if (I2CDEV_M  == LPC_I2C0)
	{
		// I2C Device Pins
		// Doesn't work
		PinCfg.Funcnum = 3;
		PinCfg.Portnum  = 0;
		PinCfg.Pinnum = 1;
		PINSEL_ConfigPin(&PinCfg);
		PinCfg.Pinnum = 0;
		PINSEL_ConfigPin(&PinCfg);
	}

	if (I2CDEV_M  == LPC_I2C1) // Works
	{
		// I2C Device Pins
		PinCfg.Funcnum = 3;
		PinCfg.Portnum  = 0;
		PinCfg.Pinnum = 1;
		PINSEL_ConfigPin(&PinCfg);
		PinCfg.Pinnum = 0;
		PINSEL_ConfigPin(&PinCfg);
	}

	if (I2CDEV_M  == LPC_I2C2) // Works
	{
		// I2C Device Pins
		PinCfg.Funcnum = 2;
		PinCfg.Portnum  = 0;
		PinCfg.Pinnum = 10;
		PINSEL_ConfigPin(&PinCfg);
		PinCfg.Pinnum = 11;
		PINSEL_ConfigPin(&PinCfg);
	}
	// Initialize Slave I2C peripheral
        I2C_Init(I2CDEV_M, 100000);

	// Enable Slave I2C operation
        I2C_Cmd(I2CDEV_M, ENABLE);

        NVIC_EnableIRQ(I2C1_IRQn);
	
	//I2C_DeInit(I2CDEV_M);
}


uint8_t Recieve_Buffer[1];

// Returns values for each row, takes values to check each column
uint8_t checkKeypad(uint8_t columnValues)
{
	uint8_t Transmit_Buffer[1];

	complete = RESET;

	Transmit_Buffer[0] = columnValues;

	I2C_M_SETUP_Type transferMCfg;

	// Start slave device
	transferMCfg.sl_addr7bit = I2CDEV_S_ADDR_KEYPAD;
	transferMCfg.tx_data = Transmit_Buffer;
	transferMCfg.tx_length = 1;
	transferMCfg.rx_data = Recieve_Buffer;
	transferMCfg.rx_length = 1;
	/*debugMessageLength = sprintf(debugMessageBuffer, "Data Sent: 0x%x\t", *Transmit_Buffer);
	_DBG(debugMessageBuffer);*/
	//_DBG("Buffer:\n\r");

	// debugMessageLength = sprintf(debugMessageBuffer, "\t0x%x\n\r", Transmit_Buffer[0]);
	// _DBG(debugMessageBuffer);
	
	// _DBG("Checking Keypad\n\r");
	
	transferMCfg.retransmissions_max = 3;
	I2C_MasterTransferData(I2CDEV_M, &transferMCfg, I2C_TRANSFER_INTERRUPT);

	// int i;
	// for (i = 0; i < 2500; ++i)
	// {
	// 	/* code */
	// }

	while (complete == RESET);
	
	return 0;
}

void Delay(unsigned long tick)
{
	int i;
	for(i=0;i<3999*tick;i++);
}

int keypadCharFromCode(char keyReceived){//Takes 8 bit key address and prints value
	char debugMessageBuffer[0xFF];
	int debugMessageLength;
	char out;
	switch(keyReceived)
	{
		case(0x77):
			out='1';
			break;
		case(0xb7):
			out='2';
			break;
		case(0xd7):
			out='3';
			break;
		case(0xe7):
			out='A';
			break;
		case(0x7b):
			out='4';
			break;
		case(0xbb):
			out='5';
			break;
		case(0xdb):
			out='6';
			break;
		case(0xeb):
			out='B';
			break;
		case(0x7d):
			out='7';
			break;
		case(0xbd):
			out='8';
			break;
		case(0xdd):
			out='9';
			break;
		case(0xed):
			out='C';
			break;
		case(0x7e):
			out='*';
			break;
		case(0xbe):
			out='0';
			break;
		case(0xde):
			out='#';
			break;
		case(0xee):
			out='D';
			break;
		default:
			out='X';
			break;

	}

	debugMessageLength = sprintf(debugMessageBuffer, "PRINT:%c\n\r",out);
	_DBG(debugMessageBuffer);

	return out;
}


// Entry point for the program
void main(void)
{
	debug_frmwrk_init();
	_DBG(	"\n\r\n\r>>>\n\r__________________________________________\n\r"
		"---------------LCD I2C Test---------------\n\r"
		"__________________________________________\n\r\n\r");


	i2c_init();

	int debugMessageLength;
	char debugMessageBuffer[0xFF];
	uint8_t colCurrentPress;
	uint8_t previousPress[]={0,0,0,0};
	unsigned int i=0;
	uint8_t columns[4]={0x7F,0xBF,0xDF,0xEF};
	while(1)
	{
		//Delay(1000);
		/*debugMessageLength = sprintf(debugMessageBuffer, "\ni:%x\n\r",(i));
		_DBG(debugMessageBuffer);*/

		colCurrentPress=checkKeypad(columns[i]);


		

		// debugMessageLength = sprintf(debugMessageBuffer, "Printing: 0x%x\n\r", colCurrentPress);
		// _DBG(debugMessageBuffer);

		colCurrentPress = *Recieve_Buffer;
		

		if(colCurrentPress!=previousPress[i])
		{
			if((colCurrentPress & 0x0F) != 0x0F)
			{
			
				if(colCurrentPress != previousPress[i])
				{
					debugMessageLength = sprintf(debugMessageBuffer, "Printing: 0x%x\n\r", colCurrentPress);
					_DBG(debugMessageBuffer);
					
				}
			}
			previousPress[i]=colCurrentPress;

		}

		(i==3)?(i=0):(i++);

	}
}