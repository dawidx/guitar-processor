// Serial code
#include "lpc17xx_uart.h"
#include "lpc17xx_pinsel.h"
#include "lpc_types.h"
#include "lpc17xx_rit.h"
#include "adc.h"
#include "dac.h"
#include "config.h"
#include "control.h"
#include "engine.c"
#include "userInterface.c"
#include <stdio.h> // For sprintf

#define PI 3.14159265

// Buffersize defined in config.h
/*
* From the new version the buffer is no longer here
*/
//uint8_t g_InputBuffer[BUFFERSIZE];
//uint16_t g_InputBufferStart = 0;	//The buffer starts at 0 and it is a 16bit (2^16 = 65536)
/* Use looping buffer, the buffer starts at 
*  g_InputBuffer[g_InputBufferStart], loops from 
*  g_InputBuffer[BUFFERSIZE] to g_InputBuffer[0] then
*  carries on to g_InputBuffer[g_InputBufferStart - 1].
*  The most recent sample is stored at 
*  g_InputBuffer[g_InputBufferStart], and the oldest at
*  g_InputBuffer[g_InputBufferStart-1].
*/

// Set initial sample rate (can be reduced if too high)
uint32_t g_sampleRate = MAX_SAMPLING_RATE;
// uint32_t sampleRate = 200000;

// Set to 0 at after input, set to 1 when output complete, set to 2 when effects computation complete.
uint8_t effectsComplete = 2;

// For Debugging, global from config.h to save memory
char debugMessageBuffer[0xFF];
int debugMessageLength;

/**
* @desc Boot_Up Boots up the entire system. It is used for initialising: ADC, DAC and UI Components
**/
void Boot_Up() {
	_DBG("UI initialised\n\r");
	// Enable interrupts for ADC conversion completing.
	Initialize_ADC(g_sampleRate); // ADC used to read input values to the input buffer. Call to adc.h
	_DBG("ADC Intialized\n\r");
	Initialize_DAC();
	_DBG("DAC Intialized\n\r");
	// Set ADC to continuously sample.
	//ADC_StartCmd (LPC_ADC, ADC_START_CONTINUOUS);
	//_DBG("ADC setup in ADC_START_CONTINUOUS mode\n\r");
	// Set ADC to start converting.
	ADC_BurstCmd (LPC_ADC, ENABLE);
	_DBG("ADC started sampling\n\r");
	// Initialise user interface
	

	Reset_Filters();
	_DBG("Reset all filters to pass through\n\r");
	
	_DBG("ADC IRQ Enabling\n\r");

	_DBG("Entered include/control.h/start()\n\r");
	UI_init();
	uint8_t adc_value, output;
	while(1) {
                // Start conversion
                ADC_StartCmd(LPC_ADC,ADC_START_NOW);
                //Wait conversion complete
                while (!(ADC_ChannelGetStatus(LPC_ADC, ADC_CHANNEL_0,ADC_DATA_DONE)));
                	adc_value = ADC_Acquire_Data();
                //Display the result of conversion on the UART0
				#ifdef MCB_LPC_1768
                	_DBG("ADC value on channel 2: ");
				#elif defined (IAR_LPC_1768)
                	_DBG("ADC value on channel 5: ");
				#endif
                _DBD32(adc_value);
                _DBG_("");
                //delay
                output = Apply_Effects(adc_value);
				DAC_Output_Data(output);

        }
        ADC_DeInit(LPC_ADC);
	// ALL STATEMENTS AFTER THIS POINT ARE NOT EXECUTED, THE INTERRUPT ROUTINE TAKES OVER
}

/**
* @desc The ADC interrupt handler, gets called every time an Analouge to Digital conversion completes. (Once each sample rate)
**
void ADC_IRQHandler (void)
{
	_DBG("Entered ADC interrupt service routine\n\r");
	// Disable the interrupt. If we don't want to get override the samples
	NVIC_DisableIRQ(ADC_IRQn);

	if (effectsComplete != 2)
	{
		// Sample rate too high, cannot compute effects in time, reduce sample rate
		g_sampleRate = reduceSampleRate(g_sampleRate);
		Update_ADC_Sample_Rate(g_sampleRate);
		effectsComplete = 1;
	}

	// _DBG("Control.c\tADC_IRQHandler\t83\tGetting Data from ADC\n\r");
	uint8_t value = ADC_Acquire_Data();

	// End data aquisition section
	NVIC_EnableIRQ(ADC_IRQn);

	// Check the user interface for any updated parameters
	// _DBG("Control.c\tADC_IRQHandler\t87\tChecking UI\n\r");
	//checkUI();

	effectsComplete = 0;
	// Apply effects
	// _DBG("Control.c\tADC_IRQHandler\t95\tStart applying effects\n\r");
	uint8_t output = Apply_Effects(value);
	
	effectsComplete = 1;
	// _DBG("Control.c\tADC_IRQHandler\t99\tEnd applying effects, output\n\r");
	// Send the value after effects applied to the DAC
	DAC_Output_Data(output);

	effectsComplete = 2;
}
*/
/**
* @desc Returns a reduced sample rate for the ADC.
* @param uint32_t samplingRate The current sampling rate of the system.
* @
**/ 
uint32_t reduceSampleRate(uint32_t samplingRate)
{
	//The minimum sampling rate is (default) 100Hz
	if (samplingRate >= MIN_SAMPLING_RATE)
	{
		debugMessageLength = sprintf(debugMessageBuffer, "Reducing sample rate from %ld Hz ", samplingRate);
		_DBG(debugMessageBuffer);

		samplingRate -= 10; //Substracting 10 for each sample

		debugMessageLength = sprintf(debugMessageBuffer, "to %ld Hz\n\r", samplingRate);
		_DBG(debugMessageBuffer);
	}
	else
	{
		// SampleRateError
		_DBG("Minimum sample rate of 100 Hz is insufficient.\n\r");
	}

	return samplingRate;
}


/**
* @desc Error procedure containing the message and the source of the error
* @param message a string containing the type of error
* @param severity a uint8_t containing values 1 being high and 8 being low severity
**/
void Error_Report(char* message, uint8_t severity)
{
	
}