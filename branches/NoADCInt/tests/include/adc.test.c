#include "../../include/adc.c";

void ADC_Testing()
{
	//Testing Convert_12_To_8
	//MAX 2^12 = 4096
	//MAX 2^8 = 255
	Equals(Convert_12_To_8(4096), 255);
	Equals(Convert_12_To_8(0, 0));
	Equals(Convert_12_To_8(12, 0));
	Equals(Convert_12_To_8(256, 128));
}