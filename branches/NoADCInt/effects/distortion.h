// #include "filter.h"
uint8_t Get_Distortion_Threshold(Filter filter);
uint8_t Get_Distortion_Effect_Value(uint8_t amplitude, Filter filter);
bool Set_Distortion_Threshold(int8_t value, Filter filter);
Filter Generate_Distortion_Effect();