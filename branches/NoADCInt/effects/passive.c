/**
* @file Passive filter just returns the input value
**/



/**
* @desc passiveEffectValue returns the input value. The function adds a bit of overhead, but it keeps consistency of the effect file structure
* @param uint8_t value the value of the sample
* @returns uint8_t The value that gets returned is exactly the same as the input value
*/
uint8_t passiveEffectValue(uint8_t value) {
	return value;
}

Filter Generate_Passive_Effect() {
	Filter filter;
	filter.ID = 0;
	Set_Effect_Volume(100, filter);
	return filter;
}

