#include <stdlib.h>
/**
* @desc struct filter stores the relevant information about filter
* int ID 
* int Parameter
**/
typedef struct filter {
	uint8_t ID;
	//First parameter is ALWAYS the volume
	uint8_t Parameter[10];
	uint8_t * Buffer;
	uint16_t Index;
} Filter;

