/**
* @file contains an implementation of the echo effect
**/

#include "echo.h"

uint8_t Get_Echo_Value(uint8_t value, uint16_t samplingRate, Filter filter) {
	//Firstly, we retrive the offset of previously stored value. This value depends on the sampling rate and the echo time
	uint16_t offset = Get_Echo_Buffer_Offset(Get_Echo_Time(filter) * samplingRate / 1000, filter);
	//we calculate the new value but considering the strength of the echo
	uint16_t new_value = ((100-Get_Echo_Strength(filter)) * value)/100 + (Get_Echo_Strength(filter) * filter.Buffer[offset])/100 ;
	//Storing the value for future use
	filter.Buffer[filter.Index] = (uint8_t)value >> 8;
	//Increasing the filter offset
	filter.Index += 1;
	return (uint8_t)new_value;
}

Filter Generate_Echo_Effect() {
	Filter filter;
	filter.ID = ECHO_EFFECT_ID;
	filter.Index = 0;
	Set_Effect_Volume(100, filter);
	Set_Echo_Strength(30, filter);
	//In miliseconds
	Set_Echo_Time(15, filter);
	uint8_t array[22000];
	filter.Buffer = array;
	return filter;
}

bool Set_Echo_Strength(uint8_t value, Filter filter) {
	if (value <= 100) {
		filter.Parameter[1] = value;
		return true;
	}
	else {
		return false;
	}
}

uint8_t Get_Echo_Strength(Filter filter) {
	return filter.Parameter[1];
}

bool Set_Echo_Time(uint8_t value, Filter filter) {
	if (value <= 500) {
		filter.Parameter[2] = value;
		return true;
	}
	else {
		return false;
	}
}

uint8_t Get_Echo_Time(Filter filter) {
	return filter.Parameter[2];
}


/**
* @desc The function calculates and returns the memory location with given offset. Note: The buffer stores from BUFFERSIZE to 0, hence the location is g_InputBufferStart PLUS the offset
* @param uint16_t offset the number of locations before the current sample
**/
uint16_t Get_Echo_Buffer_Offset(uint16_t offset, Filter filter) {
	if (offset <= 22000) {
		uint16_t location = filter.Index + offset;
		if (location >= 22000) {
			location = location - 22000;
		}
		return location;
	}

	//Error_Report("Overflow error", 4);
	return 0;
}