uint8_t Get_Echo_Value(uint8_t value, uint16_t samplingRate, Filter filter);
Filter Generate_Echo_Effect();
bool Set_Echo_Strength(uint8_t value, Filter filter);
uint8_t Get_Echo_Strength(Filter filter);
bool Set_Echo_Time(uint8_t value, Filter filter);
uint8_t Get_Echo_Time(Filter filter);
/**
* @desc The function calculates and returns the memory location with given offset. Note: The buffer stores from BUFFERSIZE to 0, hence the location is g_InputBufferStart PLUS the offset
* @param uint16_t offset the number of locations before the current sample
**/
uint16_t Get_Echo_Buffer_Offset(uint16_t offset, Filter filter);
