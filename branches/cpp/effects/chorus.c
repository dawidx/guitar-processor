/**
* @file This file contains methods and global variables related to the chorus effect
* @desc Chorus effect is like an echo effect except occurring very frequently. Just like
* voices in the choir can't sing all at exactly the same time. Some come earily some latter 
**/

/**
* @var This effect 
*/
uint8_t g_ChorusTime;

/**
* @var the Chorus matrix
* @todo: decide on the format of the matrix
*/
uint8_t[] g_ChorusMatrix;

/**
* @desc The function will generate the matrix for the chorus effect
**/
void GenerateChorusMatrix(uint8_t index) {
	
}