/**
* @file contains the full method for chorus effects
**/
class Chorus : protected Effect {

	public uint8_t ChorusTime = 50;
	public uint8_t Voice1Volume = 30;
	public uint8_t Voice2Volume = 70;

	public const int ParamNumber = 3;

	public Chorus() {
		//Initially all effects are switched off
		Effect("Chorus Effect", 1);
	}
	
	/**
	* @desc returns a x * 255 samples delay on the 
	**/
	public uint8_t getValue(int bufferIndex) {
		
	}
	
	public void Left(int parameter) {

	}

	public void Right(int parameter) {

	}

	
};