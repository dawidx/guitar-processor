/**
* @file Distortion effect compresses the pick of the wave such that they are almost square
**/
/**
* @var g_DistortionThreshold is used to set the threshold value.
*/
uint8_t g_DistortionThreshold;

/**
* @desc The value is used as a threshold.
* @param int value The threshold value 
**/
uint8_t setDistortionMaxAmplitude(uint8_t value) {
	g_DistortionThreshold = value;
}

/**
* @desc The function returns the value of the sample when it passes through distortion effect
* @param uint8_t amplitude The sample value
**/
uint8_t distortionEffectValue(uint8_t amplitude) {
	if(amplitude >= g_DistortionThreshold) {
		value = g_DistortionThreshold;
	} else if(amplitude < g_DistortionThreshold) {
		value = amplitude;
	}
	return value;
}

