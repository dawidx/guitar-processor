/**
* @file Contains the Effect class
**/
class Effect {
	/**
	* @var Name contains the value of the effect
	*/
	public char* Name;
	
	/**
	* @var Active contains a bool whether the effect is applied or not
	**/
	public bool Active;
	
	/**
	* @desc The constructor of the (abstract) class. The effect cannot be instantiated from outside.
	* @param name The name of the effect
	* @param active Allows to get 
	**/
	protected Effect(char* name, bool active) {
		*this->name = *name;
	}

	/**
	* @desc An abstract method enforcing each class that inherits form this class to
	* implement getValue method.
	* @param bufferIndex is a memory index that points to the current sample value.
	**/
	virtual uint8_t getValue(uint8_t bufferIndex);

	
};