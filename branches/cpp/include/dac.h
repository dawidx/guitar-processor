#include "lpc17xx_dac.h"

uint16_t outputValue;

void Initialize_DAC() {
	PINSEL_CFG_Type PinCfg; // Pin configuration
	PinCfg.Funcnum   = PINSEL_FUNC_2;
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Pinmode   = PINSEL_PINMODE_PULLUP;
	PinCfg.Portnum   = PINSEL_PORT_0;
	PinCfg.Pinnum    = PINSEL_PIN_26;

   	PINSEL_ConfigPin(&PinCfg);
	
   	DAC_Init(LPC_DAC);
}

void DAC_Output_Data()
{
	// Get the value read on ADC channel 1, store at the start of the buffer.
	// g_InputBuffer[((g_InputBufferStart - 1) Mod BUFFERSIZE)].
	

	//char debugMessageBuffer[0xFF];
	//int debugMessageLength;

	/*debugMessageLength = sprintf(debugMessageBuffer, "ADC Value: %dmV (0x%xmV)\n\r",
		analogue_value, analogue_value);
	_DBG(debugMessageBuffer);*/

	// Update DAC value (out of 0x400)
	outputValue = g_InputBuffer[g_InputBufferStart];
	DAC_UpdateValue( LPC_DAC, (outputValue<<2));
	// outputValue left shifted 2 bits to allow for different bit depth (8->10 bits)
}