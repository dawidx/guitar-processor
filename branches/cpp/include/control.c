// Serial code
#include "lpc17xx_uart.h"
#include "lpc17xx_pinsel.h"
#include "lpc_types.h"
#include "lpc17xx_rit.h"
#include "adc.h"
#include "dac.h"
#include "config.h"
#include "control.h"
#include "engine.c"
#include <stdio.h> // For sprintf

#define PI 3.14159265

// Buffersize defined in config.h
uint8_t g_InputBuffer[BUFFERSIZE];
uint8_t g_InputBufferStart = 1;	
/* Use looping buffer, the buffer starts at 
*  g_InputBuffer[g_InputBufferStart], loops from 
*  g_InputBuffer[BUFFERSIZE] to g_InputBuffer[0] then
*  carries on to g_InputBuffer[g_InputBufferStart - 1].
*  The most recent sample is stored at 
*  g_InputBuffer[g_InputBufferStart], and the oldest at
*  g_InputBuffer[g_InputBufferStart-1].
*/

// Set initial sample rate (can be reduced if too high)
uint32_t sampleRate = 44000;
// uint32_t sampleRate = 200000;

// Set to 0 at after input, set to 1 when output complete, set to 2 when effects computation complete.
uint8_t effectsComplete = 2;

// For Debugging
char debugMessageBuffer[0xFF];
int debugMessageLength;

/**
* Start is the first method called. It is used for 
**/

void start() {
	_DBG("Entered include/control.h/start()\n\r");
	Initialize_ADC(); // ADC used to read input values to the input buffer. Call to adc.h
	_DBG("ADC Intialized\n\r");
	Initialize_DAC();
	_DBG("DAC Intialized\n\r");
	// Set ADC to continuously sample.
	ADC_StartCmd (LPC_ADC, ADC_START_CONTINUOUS);
	_DBG("ADC setup in ADC_START_CONTINUOUS mode\n\r");
	// Set ADC to start converting.
	ADC_BurstCmd (LPC_ADC, ENABLE);
	_DBG("ADC started sampling\n\r");
	// Enable interrupts for ADC conversion completing.
	NVIC_EnableIRQ(ADC_IRQn);
}

// The ADC interrupt handler, gets called every time an A to D conversion completes. (Once each sample rate)
void ADC_IRQHandler (void)
{
	// Begin data aquisition section
	NVIC_DisableIRQ(ADC_IRQn);

	if (effectsComplete != 2)
	{
		// Sample rate too high, cannot compute effects in time, reduce sample rate
		reduceSampleRate();
	}

	ADC_Aqquzire_Data();

	// End data aquisition section
	NVIC_EnableIRQ(ADC_IRQn);

	effectsComplete = 0;
	// Send the value after effects applied to the DAC
	DAC_Output_Data();

	
	effectsComplete = 1;
	// Apply effects
	applyEffects();
	effectsComplete = 2;

}

/* Reduces the sample rate, and returns the value of the sample rate */
uint8_t reduceSampleRate()
{
	if (sampleRate >= 5)
	{
		debugMessageLength = sprintf(debugMessageBuffer, "Reducing sample rate from %ld Hz ", sampleRate);
		_DBG(debugMessageBuffer);

		sampleRate--;

		debugMessageLength = sprintf(debugMessageBuffer, "to %ld Hz\n\r", sampleRate);
		_DBG(debugMessageBuffer);
	}
	else
	{
		// SampleRateError
		_DBG("Minimum sample rate of 5 Hz is insufficient.\n\r");
	}

	UpdateADCSampleRate();

	effectsComplete = 1;
	return sampleRate;
}