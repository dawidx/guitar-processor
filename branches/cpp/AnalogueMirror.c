// Serial code
#include "lpc17xx_uart.h"		// Central include files
#include "lpc17xx_pinsel.h"
#include "lpc_types.h"
#include "lpc17xx_dac.h"
#include "lpc17xx_adc.h"
#include "lpc17xx_rit.h"
#include "debug_frmwrk.h"		// Debug messages
#include <stdio.h>
#include <math.h>			// Sine

#define PI 3.14159265

void RIT_IRQHandler(void);
double get_Number(int digits);

double currentTimePoint = 0;

uint16_t analogue_value;

// For Debugging
char debugMessageBuffer[0xFF];
int debugMessageLength;

void adc_init(void)
{
	PINSEL_CFG_Type PinCfg; // Pin configuration
	PinCfg.Funcnum   = PINSEL_FUNC_1;
   	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Pinmode   = PINSEL_PINMODE_PULLUP;
	PinCfg.Portnum   = PINSEL_PORT_0;
	PinCfg.Pinnum    = PINSEL_PIN_23;

   	PINSEL_ConfigPin(&PinCfg);
	
	// Set up the ADC sampling at 200kHz
   	ADC_Init(LPC_ADC, 200000);

   	ADC_ChannelCmd(LPC_ADC, ADC_CHANNEL_0, ENABLE);
}

void dac_init(void)
{
	PINSEL_CFG_Type PinCfg; // Pin configuration
	PinCfg.Funcnum   = PINSEL_FUNC_2;
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Pinmode   = PINSEL_PINMODE_PULLUP;
	PinCfg.Portnum   = PINSEL_PORT_0;
	PinCfg.Pinnum    = PINSEL_PIN_26;

   	PINSEL_ConfigPin(&PinCfg);
	
   	DAC_Init(LPC_DAC);
}

// The ADC interrupt handler, gets called every time an A to D conversion completes.
void ADC_IRQHandler (void)
{
	NVIC_DisableIRQ(ADC_IRQn);

	// Get the value read on ADC channel 1.
	analogue_value = ADC_ChannelGetData(LPC_ADC, ADC_CHANNEL_0);

	char debugMessageBuffer[0xFF];
	int debugMessageLength;

	/*debugMessageLength = sprintf(debugMessageBuffer, "ADC Value: %dmV (0x%xmV)\n\r",
		analogue_value, analogue_value);
	_DBG(debugMessageBuffer);*/

	// Update DAC value (out of 0x400)
	DAC_UpdateValue( LPC_DAC, (analogue_value>>2));// analogue_value right shifted 2 bits to avoid buffer overflow (10 bits only)

	NVIC_EnableIRQ(ADC_IRQn);
}




// Entry point for the program
void main(void)
{
	debug_frmwrk_init();
	_DBG(	"\n\r\n\r>>>\n\r__________________________________________\n\r"
		"---------ADC->DAC Mirroring Test----------\n\r"
		"__________________________________________\n\r"
		"\t\t(DAC is Pin 18)\n\r\n\r"		
		"\t\t(ADC is Pin 15)\n\r\n\r");

	// Set up DAC pins
	dac_init();

	// Set up ADC pins
	adc_init();

	// Set ADC to continuously sample.
	ADC_StartCmd (LPC_ADC, ADC_START_CONTINUOUS);

	// Set ADC to start converting.
	ADC_BurstCmd (LPC_ADC, ENABLE);

	// Enable interrupts for ADC conversion completing.
	NVIC_EnableIRQ(ADC_IRQn);

	// Enable interrupts globally.
	__enable_irq();
}
