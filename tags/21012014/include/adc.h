#include "lpc17xx_adc.h"
#include "lpc17xx_dac.h"
#include "config.h"
#include <stdio.h> //sprintf for debugging

void RIT_IRQHandler(void);
uint16_t valueToStore16;
uint8_t valueToStore8;

void Initialize_ADC()
{
	PINSEL_CFG_Type PinCfg; // Pin configuration
	PinCfg.Funcnum   = PINSEL_FUNC_1;
   	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Pinmode   = PINSEL_PINMODE_PULLUP;
	PinCfg.Portnum   = PINSEL_PORT_0;
	PinCfg.Pinnum    = PINSEL_PIN_23;

   	PINSEL_ConfigPin(&PinCfg);
	
	// Set up the ADC sampling at the sample rate. This rate can be varied
   	ADC_Init(LPC_ADC, sampleRate);

   	ADC_ChannelCmd(LPC_ADC, ADC_CHANNEL_0, ENABLE);
}

void UpdateADCSampleRate()
{
	ADC_DeInit(LPC_ADC);
	ADC_Init(LPC_ADC, sampleRate);
}

void ADC_Aqquire_Data()
{
	// Get the value read on ADC channel 1, store at the start of the buffer.
	// inputBuffer[((inputBufferStart - 1) Mod BUFFERSIZE)].

	valueToStore16 = ADC_ChannelGetData(LPC_ADC, ADC_CHANNEL_0);
	valueToStore8 = (uint8_t) (valueToStore16 >> 4); // Right shift to get most significant 8 bits from 12 bit ADC value

	//debugMessageLength = sprintf(debugMessageBuffer, "Sample:\t16bit:\t%d \t8bit:\t%d mV\n\r", valueToStore16, valueToStore8);
	//_DBG(debugMessageBuffer);

	if (inputBufferStart == 0)
	{
		inputBuffer[BUFFERSIZE-1] = valueToStore8;
		inputBufferStart = BUFFERSIZE-1;
	}
	else
	{
		inputBuffer[inputBufferStart - 1] = valueToStore8;
		inputBufferStart--;
	}
}