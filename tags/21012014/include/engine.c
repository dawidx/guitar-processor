/* Called for each sample, applys the effect table (which may combine
*  multiple effects), preparing it for output.
*/
void applyEffects()
{
	// Apply effects to the sample
}



/* In turn, requests an updated table for each effect that has had
*  parameters changed, or been activated or deactivated. It then combines
*  these tables, returning the combined effect table that is applied to
*  each sample.
*/
void applyChanges()
{
	// Request updated effects table from each effect

	// Combine effects tables

	// Return combined effects table

}